const {
  app,
  BrowserWindow,
  Tray,
  Menu,
  ipcMain,
  ipcRenderer,
} = require("electron");
const url = require("url");
const path = require("path");
const appIcon = "src/assets/litoral-logo.png";
let mainWindow;
let tray = null;
const gotTheLock = app.requestSingleInstanceLock();

if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, commandLine, workingDirectory) => {
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore();
      mainWindow.focus();
    }
  });
  function createWindow() {
    mainWindow = new BrowserWindow({
      width: 1000,
      height: 600,
      minWidth: 800,
      darkTheme: true,
      closable: false,
      backgroundColor: "#2e2c29",
      title: "Gestor de Pedidos | Litoral na Mesa",
      focusable: true,
      icon: appIcon,
      webPreferences: {
        nodeIntegration: true,
        textAreasAreResizable: false,
      },
    });
    mainWindow.setMenu(null);

    mainWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, `/dist/index.html`),
        protocol: "file:",
        slashes: true,
      })
    );
    mainWindow.maximize();

    // mainWindow.on("minimize", function (event) {});
    // mainWindow.on("restore", function (event) {});

    mainWindow.on("close", function (event) {
      if (!app.isQuiting) {
        event.preventDefault();
        mainWindow.hide();
      }

      return false;
    });

    mainWindow.webContents.openDevTools();
  }

  app.on("ready", () => {
    createWindow();
    createTray();
  });

  ipcMain.on("order-notify", (event, arg) => {
    mainWindow.show();
  });

  function createTray() {
    tray = new Tray(appIcon);
    var template = Menu.buildFromTemplate([
      {
        label: "Abrir gestor",
        click: () => {
          mainWindow.show();
        },
      },
      {
        label: "Encerrar sessão",
        click: () => {},
      },
      {
        label: "Sair",
        click: () => {
          app.isQuiting = true;
          app.quit();
        },
      },
    ]);

    tray.setContextMenu(template);
    tray.setToolTip("Litoral na Mesa");
    tray.on("double-click", function (event) {
      mainWindow.show();
    });
    return tray;
  }
}

module.exports = ipcRenderer;

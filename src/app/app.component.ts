import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Socket } from 'dgram';
import { AcceptOrderComponent } from './shared/dialogs/accept-order/accept-order.component';
import { NotificationService } from './shared/services/notification.service';
import { SocketOrdersService } from './shared/services/socket-orders.service';
import { warningAlert } from './shared/helpers/swal-defaults';
import { CanceledOrderComponent } from './shared/dialogs/canceled-order/canceled-order.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private socketOrderService: SocketOrdersService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {}
  title = 'portal-litoral-na-mesa';
  config: MatDialogConfig = {
    minWidth: '80vw',
    data: { orderId: 26064 },
    // disableClose: true
  };

  ngOnInit() {
    this.listenNewOrders();
    this.listenCanceledOrders();
    this.listenOrdersToExpire();
  }

  listenOrdersToExpire() {
    const audio = new Audio('../assets/sounds/order_expire.wav');
    this.socketOrderService.openedOrders$.subscribe((orders) => {
      const order = orders.find((x) => x.timeLeft_ms === 60000);
      if (order) {
        console.log('PLAY AUDIO', audio, orders);
        audio.play();
        this.openOrderDialog(order.IDOrder);
        warningAlert.fire(
          'Atenção! Existe um pedido expirando!',
          `O pedido ${order.IDOrder} irá expirar em menos de 1 minuto e será cancelado!`
        );
      }
    });
  }

  listenNewOrders() {
    const audio = new Audio('../assets/sounds/alert_1.wav');
    this.socketOrderService.$newOrder.subscribe((data) => {
      audio.play();
      this.notificationService.pushNotification(
        `Novo pedido recebido! #${data.IDOrder}`,
        this.openOrderDialog.bind(this, data.IDOrder)
      );
      this.openOrderDialog(data.IDOrder);
    });
  }

  listenCanceledOrders() {
    this.socketOrderService.canceledOrder$.subscribe((data) => {
      this.notificationService.pushNotification(
        `Pedido cancelado! #${data.IDOrder}`,
        this.canceledOrderDialog.bind(this, data.IDOrder)
      );
      this.canceledOrderDialog(data.IDOrder);
    });
  }

  openOrderDialog(orderId: number) {
    return this.dialog.open(AcceptOrderComponent, {
      minWidth: '80vw',
      data: { orderId },
      panelClass: 'accept-order-dialog'
    });
  }


  canceledOrderDialog(orderId: number) {
    return this.dialog.open(CanceledOrderComponent, {
      minWidth: '40vw',
      data: { orderId },
    });
  }
}

import { Component, OnInit } from '@angular/core';

type Params = {
  label: string;
  route: string;
  icon: string;
};

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  readonly items: Params[] = [
    {
      icon: 'file-document-outline',
      label: 'Pedidos',
      route: ''
    },
    {
      icon: 'silverware-fork-knife',
      label: 'Produtos',
      route: 'products'

    },
    {
      icon: 'star-settings-outline',
      label: 'Avaliações',
      route: ''

    },
    {
      icon: 'ticket-percent-outline',
      label: 'Cupons',
      route: 'coupons'

    },
    {
      icon: 'barcode-scan',
      label: 'Boletos',
      route: ''

    },
    {
      icon: 'storefront-outline',
      label: 'Painel Geral',
      route: ''

    },
    {
      icon: 'cog-outline',
      label: 'Config',
      route: ''

    },
    {
      icon: 'headset',
      label: 'Suporte',
      route: ''

    },
  ];
  isClosed = false
  constructor() {}

  ngOnInit(): void {}
  
  toggleSidebar(){
    this.isClosed = !this.isClosed
  }
}

import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ChangeCompanyStatusComponent } from '../../dialogs/change-company-status/change-company-status.component';
import { warningAlert } from '../../helpers/swal-defaults';
import { AuthService } from '../../services/auth.service';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  user$: Observable<any>;
  isOpened: boolean = true;

  config: MatDialogConfig = {
    minWidth: '80vw',
    data: { orderId: 26064 },
    // disableClose: true
  };
  constructor(
    public authService: AuthService,
    private dialog: MatDialog,
    public notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.user$ = this.authService.user$;
    this.subscribeNotifications();
  }

  subscribeNotifications() {
    const audio = new Audio('../../../assets/sounds/notification.wav');
    this.notificationService.$notifications.subscribe((data) => {
      if (!data.length) {
        return;
      }
      setTimeout(audio.play.bind(audio), 1000);
    });
  }

  async logout() {
    const { isConfirmed } = await warningAlert.fire({
      title: 'Deseja mesmo sair?',
      text: 'Você realmente deseja deslogar da plataforma?',
    });
    if (!isConfirmed) {
      return;
    }
    this.authService.logout();
  }

  openDialog() {
    // setInterval(() => {
    const dialogRef = this.dialog.open(
      ChangeCompanyStatusComponent,
      this.config
    );
    // }, 4000);
  }
}

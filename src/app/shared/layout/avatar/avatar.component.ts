import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {
  @Input() size: string = '10rem';
  @Input() radius: string = '100%';
  @Input() imagePath?: string;
  @Input() imageFallback = "url('./assets/images/user-image.png')";
  @Input() readAsBuffer: boolean = false;

  get path() {
    return this.imageFallback;
  }

  constructor() {}

  ngOnInit(): void {}
}

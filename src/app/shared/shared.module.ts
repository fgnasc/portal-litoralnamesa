import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateStatusPipe } from './pipes/translate-status.pipe';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTableModule } from '@angular/material/table';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatSelectModule } from '@angular/material/select';
import { MatBadgeModule } from '@angular/material/badge';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { AcceptOrderComponent } from './dialogs/accept-order/accept-order.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ChangeCompanyStatusComponent } from './dialogs/change-company-status/change-company-status.component';
import { CanceledOrderComponent } from './dialogs/canceled-order/canceled-order.component';
import { TransformDatePipe } from './pipes/transform-date.pipe';
import { ScheduleComponent } from './dialogs/schedule/schedule.component';
import { ChangeVarietyComponent } from './dialogs/change-variety/change-variety.component';
import { ToAnyPipe } from './pipes/to-any.pipe';
@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    TranslateStatusPipe,
    ToAnyPipe,
    AcceptOrderComponent,
    ChangeCompanyStatusComponent,
    CanceledOrderComponent,
    TransformDatePipe,
    ScheduleComponent,
    ChangeVarietyComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatListModule,
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatTableModule,
    MatExpansionModule,
    MatSelectModule,
    MatDialogModule,
    MatBadgeModule,
    NgxMaterialTimepickerModule,
    MatChipsModule,
    MatCheckboxModule,
    DragDropModule,
    TextFieldModule,
  ],
  exports: [
    SidebarComponent,
    HeaderComponent,
    CommonModule,
    MatIconModule,
    MatListModule,
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    TranslateStatusPipe,
    ToAnyPipe,
    MatSlideToggleModule,
    MatTableModule,
    MatExpansionModule,
    MatSelectModule,
    MatDialogModule,
    MatBadgeModule,
    NgxMaterialTimepickerModule,
    MatChipsModule,
    TransformDatePipe,
    MatCheckboxModule,
    DragDropModule,
    TextFieldModule,
  ],
})
export class SharedModule {
  constructor(iconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    iconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')
    );
  }
}

import Swal, { SweetAlertOptions } from 'sweetalert2';

const defaults: SweetAlertOptions<any, any> = {
  buttonsStyling: false,
  customClass: {
    cancelButton: 'btn btn-danger mx-2',
    confirmButton: 'btn btn-secondary mx-2',
  },
  reverseButtons: true,
  showCancelButton: true,
};

export const warningAlert = Swal.mixin({
  ...defaults,
  title: 'Voce tem certeza?',
  text: 'Deseja mesmo excluir este item?',
  cancelButtonText: 'Cancelar',
  icon: 'warning',
});

export const popup = Swal.mixin({
  ...defaults,
  showCancelButton: false,
});

export const cancelOrderPopUp = Swal.mixin({
  title: 'Justifique o cancelamento',
  text: 'Uma vez cancelado, você não poderá voltar atrás!',
  input: 'textarea',
  inputPlaceholder: 'Ex.: Motoboy indisponível ou produto fora de estoque',
  inputValidator: (value) => {
    if (!value || value.length < 10) {
      return 'O motivo deve ter no mínimo 10 caracteres';
    }
  },
  showCancelButton: true,
  cancelButtonText: 'Voltar',
  confirmButtonText: 'Cancelar pedido',
  confirmButtonColor: '#ef5350',
});

export const promptAlert = Swal.mixin({
  ...defaults,
  input: 'text',
  inputLabel: 'Informe um valor',
  showCancelButton: true,
});

export const sendByWhatsapp = Swal.mixin({
  input: 'text',
  showCancelButton: true,
  title: 'Enviar por Whatsapp',
  inputLabel: 'Número do celular',
  inputValidator: (value) => {
    if (!value || !/^\d{11}$/.test(value)) {
      return 'Infome um número válido com DDD!';
    }
  },
});

export const whatsAppAndPrint = (phone: string) =>
  Swal.mixin({
    showDenyButton: true,
    denyButtonText: 'Não',
    progressSteps: ['1', '2'],
  })
    .queue([
      {
        title: 'Deseja enviar também por Whatsapp?',
        text:
          'Acabamos de enviar a orientação para o Ambiente do seu Paciente e um e-mail com os dados de acesso!',
        input: 'text',
        inputValue: phone,
        confirmButtonText: 'Enviar',
        inputValidator: (phone) => {
          if (!phone || !/^\d{11}$/.test(phone)) {
            return 'Infome um número válido com DDD!';
          }
        },
      },
      {
        title: 'Deseja imprimir?',
        confirmButtonText: 'Imprimir',
      },
    ])
    .then((result: any) => {
      if (result.value) {
        const answers = JSON.stringify(result.value);
        Swal.fire({
          title: 'Tudo certo!',
          html:
            'Caso queira imprimir novamente, basta acessar essa orientação e clicar em "imprimir".<br /> Acabamos de enviar também a orientação para o portal do seu paciente!',
          confirmButtonText: 'Entendi!',
        });
      }
      return result;
    });

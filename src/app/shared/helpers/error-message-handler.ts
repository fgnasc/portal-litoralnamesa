export const errorHandler = error => {
    if (typeof error === "object" && error.error) {
      error = error.error;
    }
    if (typeof error === "string") {
      return error;
    }
    if (!error) {
      return "Ocorreu um erro.";
    }
    if (typeof error === "object" && error.message) {
      return error.message;
    }
    if (typeof error === "object" && error.errors instanceof Array) {
      return error.errors.map(err => (typeof err === "string" ? err : err.message || "")).join(" ");
    }
  };
  
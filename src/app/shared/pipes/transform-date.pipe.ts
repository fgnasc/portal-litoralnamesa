import { Pipe, PipeTransform } from '@angular/core';

const weekDays = [
  {
    value: 1,
    label: 'Segunda-feira',
  },
  {
    value: 2,
    label: 'Terça-feira',
  },
  {
    value: 3,
    label: 'Quarta-feira',
  },
  {
    value: 4,
    label: 'Quinta-feira',
  },
  {
    value: 5,
    label: 'Sexta-feira',
  },
  {
    value: 6,
    label: 'Sábado',
  },
  {
    value: 7,
    label: 'Domingo',
  },
];

@Pipe({
  name: 'transformDate',
})
export class TransformDatePipe implements PipeTransform {
  transform(value: any): string {
    const found = weekDays.find((x) => x.value == value);
    return found.label;
  }
}

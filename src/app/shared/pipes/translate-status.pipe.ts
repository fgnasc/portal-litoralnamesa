import { Pipe, PipeTransform } from '@angular/core';

const map = {
  Open: 'Pendente',
  Accepted: 'Em preparo',
  Dispatched: 'Despachado',
  Delivered: 'Entregue',
  Canceled: 'Cancelado',
};

@Pipe({
  name: 'translateStatus',
})
export class TranslateStatusPipe implements PipeTransform {
  transform(value: any): string {
    return Array.isArray(value)
      ? value.map((status) => map[status]).join(', ')
      : map[value];
  }
}

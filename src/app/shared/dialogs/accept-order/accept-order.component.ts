import { Component, Inject, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { OrdersService } from '../../services/orders.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { cancelOrderPopUp, warningAlert } from '../../helpers/swal-defaults';
import { HeaderComponent } from '../../layout/header/header.component';
import { IpcService } from '../../services/ipc.service';
import { AuthService } from '../../services/auth.service';
import { errorHandler } from '../../helpers/error-message-handler';
import { SocketOrdersService } from '../../services/socket-orders.service';

@Component({
  selector: 'app-accept-order',
  templateUrl: './accept-order.component.html',
  styleUrls: ['./accept-order.component.scss'],
})
export class AcceptOrderComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ordersService: OrdersService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<AcceptOrderComponent>,
    private ipc: IpcService,
    private socketOrderService: SocketOrdersService,
    private authService: AuthService
  ) {}

  order: any = null;
  timeLeft: string;
  expiredAt: Date | null = null;

  async ngOnInit(): Promise<void> {
    this.spinner.show();
    try {
      this.ipc.send('order-notify');
      this.order = await this.ordersService.getOrderById(this.data.orderId);
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao exibir pedido!');
    } finally {
      this.spinner.hide();
    }
    this.updateTimeLeft();
  }

  updateTimeLeft() {
    this.socketOrderService.openedOrders$.subscribe((openedOrders) => {
      const order = openedOrders.find((x) => x.IDOrder === this.data.orderId);
      if (order) {
        this.timeLeft = order.timeLeft;
      } else {
        this.expiredAt = new Date();
      }
    });
  }

  async decline() {
    try {
      const result = await cancelOrderPopUp.fire();
      if (!result.isConfirmed) {
        return;
      }
      const body = {
        cancelMessage: result.value,
        status: 'Canceled',
      };
      this.spinner.show();

      await this.ordersService.updateStatus(this.data.orderId, body);
      this.toastr.success('Seu pedido foi cancelado.', 'Sucesso!');
      this.dialogRef.close();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao cancelar pedido!');
    } finally {
      this.spinner.hide();
    }
  }
  async accept() {
    try {
      const body = {
        status: 'Accepted',
        deliveryTime: this.authService.user.DeliveryTime,
      };
      this.spinner.show();
      await this.ordersService.updateStatus(this.data.orderId, body);
      this.toastr.success('Seu pedido foi aceito.', 'Sucesso!');
      this.dialogRef.close();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao aceitar pedido!');
    } finally {
      this.spinner.hide();
    }
  }

  close() {
    this.dialogRef.close();
  }
}

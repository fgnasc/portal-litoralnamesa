import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { warningAlert } from '../../helpers/swal-defaults';
import { HeaderComponent } from '../../layout/header/header.component';
import { CompanyService } from '../../services/company.service';

@Component({
  selector: 'app-change-company-status',
  templateUrl: './change-company-status.component.html',
  styleUrls: ['./change-company-status.component.scss'],
})
export class ChangeCompanyStatusComponent implements OnInit {
  readonly weekDays = [
    {
      value: 1,
      label: 'Segunda-feira',
      labelShort: 'SEG',
    },
    {
      value: 2,
      label: 'Terça-feira',
      labelShort: 'TER',
    },
    {
      value: 3,
      label: 'Quarta-feira',
      labelShort: 'QUA',
    },
    {
      value: 4,
      label: 'Quinta-feira',
      labelShort: 'QUI',
    },
    {
      value: 5,
      label: 'Sexta-feira',
      labelShort: 'SEX',
    },
    {
      value: 6,
      label: 'Sábado',
      labelShort: 'SÁB',
    },
    {
      value: 7,
      label: 'Domingo',
      labelShort: 'DOM',
    },
  ];

  readonly menus = ['Café da manhã', 'Almoços', 'Jantas', 'Lanches'];
  @ViewChild('openHour') openTimePicker;
  @ViewChild('closeHour') closeTimePicker;
  form: FormArray = this.fb.array([
    new FormGroup({
      Name: new FormControl(null),
      Opens: new FormControl(null),
      Closes: new FormControl(null),
      Day: new FormControl([]),
    }),
  ]);
  darkTheme: any = {
    container: {
      bodyBackgroundColor: '#424242',
      buttonColor: '#fff',
    },
    dial: {
      dialBackgroundColor: '#555',
    },
    clockFace: {
      clockFaceBackgroundColor: '#555',
      clockHandColor: '#9fbd90',
      clockFaceTimeInactiveColor: '#fff',
    },
  };
  openings: any[] = null;

  get hourForm(): FormGroup {
    return new FormGroup({
      Name: new FormControl(null),
      Opens: new FormControl(null),
      Closes: new FormControl(null),
      Day: new FormControl([]),
    });
  }

  constructor(
    private companyService: CompanyService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<HeaderComponent>
  ) {}

  async ngOnInit(): Promise<void> {
    this.openings = await this.companyService.getOpeningHours();
    while (this.form.length < this.openings.length) {
      this.form.push(this.hourForm);
    }
    this.form.patchValue(this.openings);
  }

  save() {}

  showHours() {
    console.log(this.form.value);
  }

  add() {
    this.form.push(this.hourForm);
    this.showHours()
  }
  async deleteHour(index) {
    const resp = await warningAlert.fire({
      title: 'Deseja mesmo remover este horario?',
      text: null,
    });
    if (!resp.isConfirmed) {
      return;
    }
    this.form.controls.splice(index, 1);
    console.log(this.form.value);
    
  }
  close() {
    this.dialogRef.close();
  }
}

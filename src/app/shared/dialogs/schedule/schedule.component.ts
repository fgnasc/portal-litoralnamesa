import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { errorHandler } from '../../helpers/error-message-handler';
import { darkTheme } from '../../helpers/material-timepicker-theme';
import { warningAlert } from '../../helpers/swal-defaults';
import { CompanyService } from '../../services/company.service';
import { IpcService } from '../../services/ipc.service';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {
  scheduleId: number = null;
  timePickerTheme = darkTheme;
  form: FormGroup = this.fb.group({
    IDSchedule: [null],
    Name: [null],
    schDate: [null],
    Open: [null, Validators.required],
    IDMenu: [null],
    Close: [null, Validators.required],
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private dialogRef: MatDialogRef<ScheduleComponent>,
    private fb: FormBuilder,
    private companyService: CompanyService
  ) {}

  async ngOnInit(): Promise<void> {
    try {
      this.spinner.show();
      this.form.controls.IDSchedule.setValue(this.data.scheduleId + 1);
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao buscar pedido!');
    } finally {
      this.spinner.hide();
    }
  }

  async save() {
    this.spinner.show();
    try {
      await this.companyService.insertSchedule(this.form.value);
      this.toastr.success('Seu agendamento foi salvo.', 'Sucesso!');
      this.dialogRef.close();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao salvar horários!');
    } finally {
      this.spinner.hide();
    }
  }

  close() {
    this.dialogRef.close();
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { errorHandler } from '../../helpers/error-message-handler';
import { IpcService } from '../../services/ipc.service';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-canceled-order',
  templateUrl: './canceled-order.component.html',
  styleUrls: ['./canceled-order.component.scss']
})
export class CanceledOrderComponent implements OnInit {

  order: any = null;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ipc: IpcService,
    private ordersService: OrdersService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private dialogRef: MatDialogRef<CanceledOrderComponent>,

  ) { }

  async ngOnInit(): Promise<void> {
    this.spinner.show();
    try {
      this.ipc.send('order-notify');
      this.order = await this.ordersService.getOrderById(this.data.orderId);
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao buscar pedido!');
    } finally {
      this.spinner.hide();
    }
  }

  close() {
    this.dialogRef.close();
  }

}

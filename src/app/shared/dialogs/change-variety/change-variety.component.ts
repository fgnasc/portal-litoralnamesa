import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { errorHandler } from '../../helpers/error-message-handler';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-change-variety',
  templateUrl: './change-variety.component.html',
  styleUrls: ['./change-variety.component.scss'],
})
export class ChangeVarietyComponent implements OnInit {
  variety: any = null;
  actualGroupIndex: number = 0;
  form: FormGroup = this.fb.group({
    VarietyName: [null],
    VarietyValue: [''],
    VarietyDescription: [''],
    VarietyStatus: [0],
    SizeName: [''],
    MaxOptions: [0],
    MinOptions: 0,
    GroupOptions: this.fb.array([]),
  });

  get groupOptionsFormArray(): FormArray {
    return this.form.controls.GroupOptions as FormArray;
  }

  get GroupOptionsFormGroup(): FormGroup {
    return new FormGroup({
      IDGroupOption: new FormControl(0),
      GroupName: new FormControl(''),
      Options: new FormArray([]),
    });
  }

  get optionFormGroup(): FormGroup {
    return new FormGroup({
      IDOption: new FormControl(0),
      Max: new FormControl(0),
      Min: new FormControl(0),
      OptionStatus: new FormControl(0),
      OptionValue: new FormControl(0),
      VarietyOptionsStatus: new FormControl(0),
      Required: new FormControl(0),
      OptionName: new FormControl(''),
    });
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ChangeVarietyComponent>,
    private fb: FormBuilder,
    private productService: ProductsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  async ngOnInit(): Promise<void> {
    this.variety = this.data.variety;

    if (!this.variety.GroupOptions) {
      this.form.patchValue(this.variety);
      return;
    }
    this.variety.GroupOptions.forEach((group, i) => {
      const groupOption = this.GroupOptionsFormGroup;
      this.groupOptionsFormArray.push(groupOption);

      group.Options.forEach(() => {
        (this.form.controls.GroupOptions as any).controls[
          i
        ].controls.Options.push(this.optionFormGroup);
      });
    });
    this.form.patchValue(this.variety);
  }

  close() {
    this.dialogRef.close();
  }

  async save() {
    this.spinner.show();
    try {
      await this.productService.updateVariety(this.form.value);
      this.toastr.success('ok');
    } catch (error) {
      this.toastr.error(errorHandler(error));
    } finally {
      this.spinner.hide();
    }
  }
}

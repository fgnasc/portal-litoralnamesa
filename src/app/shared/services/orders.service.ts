import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  private baseUrl = `${environment.apiUrl}/orders`;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    observe: 'response',
  });

  constructor(private http: HttpClient, private router: Router) {}

  getOrders(formValue): Promise<any> {
    let params = '';
    if (formValue.query) params += `query=${formValue.query}&`;
    if (formValue.date)
      params += `date=${(formValue.date as Date).toISOString().substr(0, 10)}&`;
    if (formValue.status.length > 0)
      params += `status=${formValue.status.join()}`;

    return this.http
      .get<any[]>(`${this.baseUrl}/?${params}`, {
        headers: this.headers,
      })
      .toPromise();
  }
  getOrderById(orderId): Promise<any> {
    return this.http
      .get<any[]>(`${this.baseUrl}/${orderId}`, {
        headers: this.headers,
      })
      .toPromise();
  }
  getQuantity(customerId): Promise<any> {
    return this.http
      .get<any[]>(`${this.baseUrl}/get-quantity-orders/${customerId}`, {
        headers: this.headers,
      })
      .toPromise();
  }

  updateStatus(orderId, body): Promise<any> {
    return this.http
      .post<any[]>(`${this.baseUrl}/${orderId}/update-status`, body, {
        headers: this.headers,
      })
      .toPromise();
  }
}

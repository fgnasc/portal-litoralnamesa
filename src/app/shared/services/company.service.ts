import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  // opening-hours
  private baseUrl = `${environment.apiUrl}/company`;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {}

  getOpeningHours() {
    return this.http
      .get<any[]>(`${this.baseUrl}/opening-hours`, {
        headers: this.headers,
      })
      .toPromise();
  }

  getSchedules() {
    return this.http
      .get<any[]>(`${this.baseUrl}/schedules`, {
        headers: this.headers,
      })
      .toPromise();
  }

  insertHours(body: any) {
    return this.http
      .post<any>(`${this.baseUrl}/default`, body, {
        headers: this.headers,
      })
      .toPromise();
  }
  insertSchedule(body: any) {
    return this.http
      .post<any>(`${this.baseUrl}/schedule`, body, {
        headers: this.headers,
      })
      .toPromise();
  }
  deleteSchedule(id) {
    return this.http
      .delete<any>(`${this.baseUrl}/schedule/${id}`, {
        headers: this.headers,
      })
      .toPromise();
  }
  setAutomaticSchedule(status) {
    return this.http
      .patch<any>(`${this.baseUrl}/automatic-schedule/${status}`, {
        headers: this.headers,
      })
      .toPromise();
  }
}

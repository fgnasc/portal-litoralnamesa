import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { SocketOrdersService } from './socket-orders.service';

export type Permission = {
  _id: string;
  name: string;
  label: string;
  write: boolean;
  read: boolean;
  delete: boolean;
};

export type Theme =  'LIGHT' | 'DARK';

const jwt = new JwtHelperService();
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly userSubject = new BehaviorSubject<any>(null);
  private baseUrl = `${environment.apiUrl}/auth`;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  private get tokenKey(): string {
    return environment.tknKey;
  }
  private get userKey(): string {
    return environment.usrKey;
  }

  get user(): any {
    return JSON.parse(window.localStorage.getItem(this.userKey));
  }

  set user(user) {
    window.localStorage.setItem(this.userKey, JSON.stringify(user));
    this.userSubject.next(user);
  }

  get token() {
    return window.localStorage.getItem(this.tokenKey);
  }
  set token(token: string) {
    window.localStorage.setItem(this.tokenKey, token);
  }


  get theme(): Theme {
    return window.localStorage.getItem('theme') as Theme || 'LIGHT';
  }
  set theme(theme: Theme) {
    window.localStorage.setItem('theme', theme);
  }

  get user$(): BehaviorSubject<any> {
    return this.userSubject;
  }

  get isAdmin(): boolean {
    return !!this.user?.company?.admin;
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    private socketOrdersService: SocketOrdersService
  ) {
    this.decodeAndNotify();
    if (this.isLogged()) {
      this.socketOrdersService.connectSocket(this.token);
    }
    this.setThemeClass();
  }

  authenticate(body) {
    return this.http
      .post<any>(`${this.baseUrl}/login`, body, { headers: this.headers, observe: 'response' })
      .pipe(
        tap((res) => {
          this.token = res.body.token;
          this.user = res.body.user;
          this.socketOrdersService.connectSocket(res.body.token);
        })
      )
      .toPromise();
  }

  hasToken() {
    return !!this.token;
  }


  isLogged() {
    const isExpired = this.isExpired();
    const token = this.hasToken();
    if (!token || isExpired) {
      console.warn(
        !token
          ? `Usuário não autenticado.`
          : `Token expirado, redirecionando para login...`
      );
      this.removeUser();
      return false;
    }
    return token && !isExpired;
  }

  logout() {
    this.removeUser();
    window.localStorage.clear();
    this.router.navigate(['/auth/login']);
    this.socketOrdersService.disconnect();
  }

  toggleTheme() {
    this.theme = (this.theme === 'LIGHT') ? 'DARK' : 'LIGHT';
    this.setThemeClass();
  }

  setThemeClass() {
    document.body.classList.add(this.theme.toLowerCase() + '-theme');
    document.body.classList.remove((this.theme == 'DARK' ? 'light' : 'dark') + '-theme');
  }

  private decodeAndNotify() {
    const user = JSON.parse(window.localStorage.getItem(this.userKey));
    if (user) {
      this.user = user;
    }
  }

  removeUser() {
    window.localStorage.removeItem(this.userKey);
    this.userSubject.next(null);
  }

  getDecodedToken() {
    return jwt.decodeToken(this.token);
  }

  isExpired() {
    return jwt.isTokenExpired(this.token);
  }

  removeToken() {
    window.localStorage.removeItem(this.tokenKey);
  }
}

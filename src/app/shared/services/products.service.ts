import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private baseUrl = `${environment.apiUrl}/products`;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {}

  getProducts() {
    return this.http
      .get<any>(`${this.baseUrl}/products`, {
        headers: this.headers,
      })
      .toPromise();
  }
  getProductsPopulated() {
    return this.http
      .get<any>(`${this.baseUrl}/products-populated`, {
        headers: this.headers,
      })
      .toPromise();
  }
  getGroupOptions() {
    return this.http
      .get<any>(`${this.baseUrl}/group-options`, {
        headers: this.headers,
      })
      .toPromise();
  }
  getSizeInfo() {
    return this.http
      .get<any>(`${this.baseUrl}/size-info`, {
        headers: this.headers,
      })
      .toPromise();
  }
  changeStatus(id, status) {
    return this.http
      .patch<any>(
        `${this.baseUrl}/product-status`,
        { id, status },
        {
          headers: this.headers,
        }
      )
      .toPromise();
  }

  updateVariety(body) {
    return this.http
      .put<any>(
        `${this.baseUrl}/update-variety`,
        { body },
        {
          headers: this.headers,
        }
      )
      .toPromise();
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notifications: Notification[] = [];
  private _unreadNotifications: number;
  readonly $notifications: BehaviorSubject<Notification[]> = new BehaviorSubject(this.notifications); 
  
  get unreadNotifications() {
    return this.notifications.filter(x => !x.isRead).length;
  }

  constructor() { }

  pushNotification(message: string, onClick: (x: Notification) => {}) {
    const notification = new Notification(message, onClick);
    this.notifications.push(notification);
    this.$notifications.next(this.notifications);
  }

  showNotifications() {
    this.notifications.forEach(x => x.read());
    this.$notifications.next(this.notifications);
  }

}

class Notification {
  public isRead: boolean = false;
  readonly createdAt = new Date();

  constructor(public readonly message: string, private readonly onClick?: (x: Notification) => {}) {

  }

  seeDetails() {
    this.read();
    if (typeof this.onClick === 'function') {
      this.onClick(this);
    }
  }

  read() {
    this.isRead = true;
  }


}
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { io } from 'socket.io-client';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NotificationService } from './notification.service';
import { BehaviorSubject, Subject } from 'rxjs';

export interface OpenedOrder {
  IDOrder: number;
  timeLeft: string;
  timeLeft_ms: number;
}
export interface CanceledOrder {
  IDOrder: number;
  IDCompany: string;
  cancelMessage: number;
}
@Injectable({
  providedIn: 'root',
})
export class SocketOrdersService {
  public socketConnection: any;
 
  readonly #openedOrders: OpenedOrder[] = [];
  public readonly openedOrders$ = new BehaviorSubject<OpenedOrder[]>(
    this.#openedOrders
  );
  public readonly $newOrder = new Subject<any>();
  public readonly canceledOrder$ = new Subject<CanceledOrder>();

  constructor() {}

  get isConnected(): boolean {
    return this.socketConnection?.connected;
  }

  async connectSocket(authToken: string) {
    this.socketConnection = io(environment.apiRoot + '/orders', {
      auth: { token: authToken },
    });

    this.socketConnection.on('connect', () => {
      console.log(`Socket ${this.socketConnection.id} conectado!`);
    });

    this.socketConnection.on('new-order', (data) => {
      this.$newOrder.next(data);
    });
    
    this.socketConnection.on('cancel-order', (data) => {
      console.log('canceled', data);
      this.canceledOrder$.next(data);
    });

    this.socketConnection.on('opened-order-time-left', (data: OpenedOrder) => {
      const orderIndex = this.#openedOrders.findIndex(
        (x) => x.IDOrder === data.IDOrder
      );
      if (orderIndex > -1) {
        if (data.timeLeft_ms <= 0) {
          this.#openedOrders.splice(orderIndex, 1);
        } else {
          Object.assign(this.#openedOrders[orderIndex], data);
        }
      } else {
        this.#openedOrders.push(data);
      }
      this.openedOrders$.next(this.#openedOrders);
    });

  }

  async disconnect() {
    console.log(this.socketConnection);
    if (this.socketConnection?.connected) {
      this.socketConnection.disconnect();
    }
  }
}

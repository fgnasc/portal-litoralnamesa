import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { environment } from "src/environments/environment";

const jwt = new JwtHelperService();
@Injectable({
  providedIn: "root",
})
export class TokenService {

  private get tokenKey(): string {
    return environment.tknKey;
  }

  hasToken() {
    return !!this.getToken();
  }

  setToken(token: string) {
    window.localStorage.setItem(this.tokenKey, token);
  }

  getToken() {
    return window.localStorage.getItem(this.tokenKey);
  }

  getDecodedToken() {
    return jwt.decodeToken(this.getToken());
  }

  getExpToken() {
    const token = this.getToken();
    if (!token) return new Date("2000-01-01");
    return jwt.getTokenExpirationDate(token);
  }

  isExpired() {
    return jwt.isTokenExpired(this.getToken());
  }

  removeToken() {
    window.localStorage.removeItem(this.tokenKey);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () =>
      import('./pages/public/public.module').then((m) => m.PublicModule),
  },
  {
    path: '',
    loadChildren: () =>
      import('./pages/private/private.module').then((m) => m.PrivateModule),
  },
  { path: 'products', loadChildren: () => import('./pages/private/products/products.module').then(m => m.ProductsModule) },
  { path: 'products', loadChildren: () => import('./pages/private/products/products.module').then(m => m.ProductsModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

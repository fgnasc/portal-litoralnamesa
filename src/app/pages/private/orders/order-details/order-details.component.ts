import {
  Component,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  EventEmitter,
} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { errorHandler } from 'src/app/shared/helpers/error-message-handler';
import {
  cancelOrderPopUp,
  warningAlert,
} from 'src/app/shared/helpers/swal-defaults';
import { AuthService } from 'src/app/shared/services/auth.service';
import { OrdersService } from 'src/app/shared/services/orders.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
})
export class OrderDetailsComponent implements OnInit, OnChanges {
  @Input() orderId: any;
  @Output() refreshOrderList = new EventEmitter();

  order: any;
  displayedColumns: string[] = [
    'Quantity',
    'ProductName',
    'VarietyName',
    'Size',
    'Options',
    'Notes',
    'productPrice',
  ];

  constructor(
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private ordersService: OrdersService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.orderId) {
      this.getOrderById();
    }
  }

  async getOrderById() {
    if (!this.orderId) {
      this.order = null;
      return;
    }
    this.spinner.show();
    try {
      this.order = await this.ordersService.getOrderById(this.orderId);
      this.order.orderQuantity = await this.ordersService.getQuantity(this.order.IDCustomer)
      
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao buscar pedido!');
    } finally {
      this.spinner.hide();
    }
  }

  async decline() {
    try {
      const result = await cancelOrderPopUp.fire();
      if (!result.isConfirmed) {
        return;
      }
      const body = {
        cancelMessage: result.value,
        status: 'Canceled',
      };
      this.spinner.show();

      await this.ordersService.updateStatus(this.order.IDOrder, body);
      this.toastr.success('Seu pedido foi cancelado.', 'Sucesso!');
      this.refreshOrderList.emit();
      this.getOrderById();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao cancelar pedido!');
    } finally {
      this.spinner.hide();
    }
  }
  async accept() {
    try {
      const body = {
        status: 'Accepted',
        deliveryTime: this.authService.user.DeliveryTime,
      };
      this.spinner.show();
      await this.ordersService.updateStatus(this.order.IDOrder, body);
      this.toastr.success('Seu pedido foi aceito.', 'Sucesso!');
      this.refreshOrderList.emit();
      this.getOrderById();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao aceitar pedido!');
    } finally {
      this.spinner.hide();
    }
  }
  async dispatch() {
    try {
      const resp = await warningAlert.fire({
        buttonsStyling: true,
        title: 'Deseja mesmo despachar?',
        input: 'text',
        inputPlaceholder: 'Insira o nome do entregador',
        text: 'Deseja mesmo liberar este pedido para entrega',
        showConfirmButton: true,
        confirmButtonText: 'Despachar',
        confirmButtonColor: '#5bbb60',
      });
      if (!resp.isConfirmed) {
        return;
      }
      const body = {
        status: 'Dispatched',
        Courier: resp.value,
      };
      this.spinner.show();
      await this.ordersService.updateStatus(this.order.IDOrder, body);
      this.toastr.success('Seu pedido foi aceito.', 'Sucesso!');
      this.refreshOrderList.emit();
      this.getOrderById();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao aceitar pedido!');
    } finally {
      this.spinner.hide();
    }
  }
}

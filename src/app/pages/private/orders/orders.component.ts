import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { OrdersService } from 'src/app/shared/services/orders.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { errorHandler } from 'src/app/shared/helpers/error-message-handler';
import { SocketOrdersService, OpenedOrder } from 'src/app/shared/services/socket-orders.service';

type Status = { type: string; label: string; active: boolean };
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {
  form: FormGroup = this.fb.group({
    query: [null],
    date: [new Date(), Validators.required],
    status: [[]],
  });
  showFilters: boolean = false;
  orders: any[] = [];
  paymentInfo: any = { online: 0, offline: 0 };

  selectedOrderId: number = null;
  openedOrders: OpenedOrder[] = []; 

  get statusVal() {
    return this.form.controls.status.value as string[];
  }

  availableStatus: Status[] = [
    { type: 'Open', label: 'Pendente', active: false },
    { type: 'Accepted', label: 'Em preparo', active: false },
    { type: 'Dispatched', label: 'Despachado', active: false },
    { type: 'Delivered', label: 'Entregue', active: false },
    { type: 'Canceled', label: 'Cancelado', active: false },
  ];

  constructor(
    private fb: FormBuilder,
    private ordersService: OrdersService,
    private spinner: NgxSpinnerService,
    private socketOrderService: SocketOrdersService,
    private toastr: ToastrService,
  ) {}

  async ngOnInit(): Promise<void> {
    this.getOrders();
    this.form.valueChanges
      .pipe(distinctUntilChanged(), debounceTime(500))
      .subscribe(() => this.getOrders());
    this.updateTimeLeft();
    this.listenCanceledOrders();
  }
  
  listenCanceledOrders() {
    this.socketOrderService.canceledOrder$.subscribe((data) => {
      this.getOrders();
      if (this.selectedOrderId === data.IDOrder) {
        this.selectedOrderId = null;
        setTimeout(() => this.selectedOrderId = data.IDOrder, 100);
      }
    });
  }

  updateTimeLeft() {
    this.socketOrderService.openedOrders$.subscribe((openedOrders) => {
      this.openedOrders = openedOrders;
      this.bindOpenedOrdersTimeLeft();
    });
  }
  
  bindOpenedOrdersTimeLeft() {
    this.openedOrders.forEach(openedOrder => {
      const order = this.orders.find(order => openedOrder.IDOrder === order.IDOrder);
      if (order) {
        order.timeLeft = openedOrder.timeLeft;
      }
    })
  }

  setToday() {
    this.form.controls.date.setValue(new Date());
  }

  async getOrders() {
    try {
      this.paymentInfo = { online: 0, offline: 0 };
      this.spinner.show();
      this.orders = await this.ordersService.getOrders(this.form.value);
      this.orders.forEach((order) => {
        this.paymentInfo[order.PaymentType] += parseFloat(order.Total);
      });
      this.bindOpenedOrdersTimeLeft();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao buscar pedidos!');
    } finally {
      this.spinner.hide();
    }
  }


  // addStatusFilter(status: Status) {
  //   status.active = !status.active;
  //   if (this.statusForm.some((val) => val == status.type)) {
  //     this.statusForm.splice(this.statusForm.indexOf(status.type), 1);
  //   } else {
  //     this.statusForm.push(status.type);
  //   }
  //   this.form.controls.status.setValue(this.form.value.status.map(x => x));

  // }
}

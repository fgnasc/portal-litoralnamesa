import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { CompanyStatusComponent } from './company-status/company-status.component';
import { CouponsComponent } from './coupons/coupons.component';
import { OrdersComponent } from './orders/orders.component';
import { PrivateComponent } from './private.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'home', component: OrdersComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'company-status', component: CompanyStatusComponent },
      { path: 'coupons', component: CouponsComponent },
      { path: 'products', component: ProductsComponent }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateRoutingModule {}

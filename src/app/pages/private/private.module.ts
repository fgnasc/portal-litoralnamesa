import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrdersComponent } from './orders/orders.component';
import { CouponsComponent } from './coupons/coupons.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { CompanyStatusComponent } from './company-status/company-status.component';

@NgModule({
  declarations: [PrivateComponent, OrdersComponent, CouponsComponent, OrderDetailsComponent, NotificationsComponent, CompanyStatusComponent],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    SharedModule
  ]

})
export class PrivateModule { }

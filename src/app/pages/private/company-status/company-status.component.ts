import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { ScheduleComponent } from 'src/app/shared/dialogs/schedule/schedule.component';
import { errorHandler } from 'src/app/shared/helpers/error-message-handler';
import { darkTheme } from 'src/app/shared/helpers/material-timepicker-theme';
import { warningAlert } from 'src/app/shared/helpers/swal-defaults';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CompanyService } from 'src/app/shared/services/company.service';

@Component({
  selector: 'app-company-status',
  templateUrl: './company-status.component.html',
  styleUrls: ['./company-status.component.scss'],
})
export class CompanyStatusComponent implements OnInit {
  readonly weekDays = [
    {
      value: 1,
      label: 'Segunda-feira',
      labelShort: 'Seg',
    },
    {
      value: 2,
      label: 'Terça-feira',
      labelShort: 'Ter',
    },
    {
      value: 3,
      label: 'Quarta-feira',
      labelShort: 'Qua',
    },
    {
      value: 4,
      label: 'Quinta-feira',
      labelShort: 'Qui',
    },
    {
      value: 5,
      label: 'Sexta-feira',
      labelShort: 'Sex',
    },
    {
      value: 6,
      label: 'Sábado',
      labelShort: 'Sab',
    },
    {
      value: 7,
      label: 'Domingo',
      labelShort: 'Dom',
    },
  ];
  user$: Observable<any> = null;
  userData: any = null;
  readonly menus = ['Café da manhã', 'Almoços', 'Jantas', 'Lanches'];
  @ViewChild('openHour') openTimePicker;
  @ViewChild('closeHour') closeTimePicker;
  formHour: FormArray = this.fb.array([this.hourGroup]);
  formSchedule: FormArray = this.fb.array([this.schGroup]);
  flatted: boolean = false;
  timePickerTheme = darkTheme;
  openings: any[] = null;
  schedules: any[] = [];

  get hourGroup(): FormGroup {
    return this.fb.group({
      IDOpening: [null],
      Name: [null],
      Open: [null],
      IDMenu: [null],
      Close: [null],
      Day: [[]],
    });
  }
  get schGroup(): FormGroup {
    return this.fb.group({
      IDSchedule: [null],
      Name: [null],
      schDate: [null],
      IDMenu: [null],
      Close: [null],
      Open: [null],
    });
  }

  constructor(
    private companyService: CompanyService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog,
    private authService: AuthService
  ) {
    this.user$ = authService.user$;
  }

  async ngOnInit(): Promise<void> {
    try {
      this.spinner.show();
      this.user$.subscribe( user => this.userData = user)
      const [openings, schedules] = await Promise.all([
        this.companyService.getOpeningHours(),
        this.companyService.getSchedules(),
      ]);
      this.openings = openings;
      this.schedules = schedules;

      while (this.formHour.length < this.openings.length) {
        this.formHour.push(this.hourGroup);
      }
      while (this.formSchedule.length < this.schedules.length) {
        this.formSchedule.push(this.schGroup);
      }
      this.formHour.patchValue(this.openings);
      this.formSchedule.patchValue(this.schedules);
      this.formHour.disable();
      this.formSchedule.disable();
    } catch (error) {
      this.toastr.error(errorHandler(error));
    } finally {
      this.spinner.hide();
    }
  }

  async saveDefault() {
    this.spinner.show();
    try {
      await this.companyService.insertHours(this.formHour.value);
      this.toastr.success('Seus horários foram alterados.', 'Sucesso!');
      this.formHour.disable();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao salvar horários!');
    } finally {
      this.spinner.hide();
    }
  }
  async saveSchedule() {
    this.spinner.show();
    try {
      await this.companyService.insertSchedule(this.formSchedule.value);
      console.log(this.formHour);

      this.toastr.success('Seus horários foram alterados.', 'Sucesso!');
      this.formSchedule.disable();
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao salvar horários!');
    } finally {
      this.spinner.hide();
    }
  }

  editDeliveryTime() {
    console.log(this);
  }

  addDefault() {
    this.formHour.push(this.hourGroup);
  }

  openScheduleModal() {
    const scheduleId = this.schedules.reduce((ac, x) => {
      return x.IDSchedule > ac ? x.IDSchedule : ac;
    }, 0);
    return this.dialog.open(ScheduleComponent, {
      minWidth: '500px',
      data: { scheduleId },
    });
  }

  async deleteDefault(id, index) {
    const resp = await warningAlert.fire({
      title: 'Deseja mesmo remover este horário?',
      text: null,
    });
    if (!resp.isConfirmed) {
      return;
    }
    this.spinner.show();
    try {
      await this.companyService.deleteSchedule(id);
      this.formSchedule.controls.splice(index, 1);
      this.toastr.success('Seus horários foram alterados.', 'Sucesso!');
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao salvar horários!');
    } finally {
      this.spinner.hide();
    }
  }

  disableHour() {
    this.formHour.disable();
  }
}

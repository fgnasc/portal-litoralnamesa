import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { ChangeVarietyComponent } from 'src/app/shared/dialogs/change-variety/change-variety.component';
import { errorHandler } from 'src/app/shared/helpers/error-message-handler';
import { warningAlert } from 'src/app/shared/helpers/swal-defaults';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CompanyService } from 'src/app/shared/services/company.service';
import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed',
        style({ height: '0px', minHeight: '0', overflow: 'hidden' })
      ),
      state('expanded', style({ height: '*', overflow: 'unset' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class ProductsComponent implements OnInit {
  user$: Observable<any> = null;
  columnsToDisplay = ['OrderList', 'ProductName'];
  // columnsToDisplay = [
  //   { key: 'OrderList', label: 'Ordem' },
  //   { key: 'ProductName', label: 'Nome do produto' },
  // ];
  expandedElement: any[];
  userData: any;
  products: any[];
  groupOptions: any[];
  sizeInfo: any[];
  collapsed: boolean;
  constructor(
    private companyService: CompanyService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog,
    private authService: AuthService,
    private productsService: ProductsService
  ) {
    this.user$ = authService.user$;
  }

  async ngOnInit(): Promise<void> {
    this.user$.subscribe((user) => (this.userData = user));

    this.products = await this.productsService.getProductsPopulated();

    console.log(this.products);
  }

  async changeProductStatus(id, status) {
    try {
      const title = `Deseja ${
        status == '1' ? 'desabilitar' : 'habilitar'
      } este produto?`;
      const { isConfirmed } = await warningAlert.fire({
        title,
        text: '',
      });
      if (!isConfirmed) {
        return;
      }
      this.spinner.show();
      await this.productsService.changeStatus(id, status);
    } catch (error) {
      this.toastr.error(errorHandler(error), 'Falha ao salvar horários!');
    } finally {
      this.spinner.hide();
    }
  }

  showVariety(variety) {
    console.log(variety);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.products, event.previousIndex, event.currentIndex);
  }
  productDetails(product) {
    console.log(product);
  }

  openVarietyModal(variety) {
    return this.dialog.open(ChangeVarietyComponent, {
      minWidth: '1000px',
      data: { variety },
    });
  }
}

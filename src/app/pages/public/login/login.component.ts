import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  form: FormGroup = this.fb.group({
    email: [null, Validators.required],
    password: [null, Validators.required],
  });

  constructor(
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void { }

  async submit() {
    try {
      this.spinner.show();
      const result = await this.authService.authenticate(this.form.value);
      this.router.navigate(['/home']);
    } catch (error) {
      this.toastr.error('Email ou senha não foram encontrados', 'Falha na autenticação!');
    } finally {
      this.spinner.hide();
    }
  }

  forgetPassword() {
    this.router.navigate(['auth/forgot-password']);
  }
}
